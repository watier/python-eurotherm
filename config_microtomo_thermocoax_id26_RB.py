import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
qapp = qt.QApplication([])

keepgoing = True

def printTemp(myEuro):
    print ("Temperature via callback {temp}".format(temp=myEuro.temperature)) 


def rampTest(myEuro):
    global keepgoing
    myEuro.Ramp_Rate_Disable = 0
    myEuro.rampRate = 5
    
    startingTemp = 40
    maxTemp = 260
    step = 5
    
    maxTime = 15*60

    while(keepgoing):
        myEuro.setpoint = 10
        while myEuro.temperature > startingTemp:
            print("Cooling", startingTemp, str(myEuro.temperature))
            time.sleep(10)
        for s in range(startingTemp, maxTemp, step):
                starttime=time.time()
                myEuro.setpoint = s
                time.sleep(maxTime)


def findP(myEuro, updateThread, maxTime=600, startingP = 1450, endingP = 100, stepP=-50, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for p in range(startingP, endingP, stepP):
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found p ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def findD(myEuro, updateThread, maxTime=600, startingD = 0, endingD = 200, stepD=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for d in range(startingD, endingD, stepD):
            if not keepgoing: break
            print("D", d)
            myEuro.D = d
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found d ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def findI(myEuro, updateThread, maxTime=600, startingI = 0, endingI = 200, stepI=10, targetTemp=100 ):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
        myEuro.setpoint = targetTemp
            
        for i in range(startingI, endingI, stepI):
            if not keepgoing: break
            print("I", i)
            myEuro.I = i
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid)
        updateThread.pidPlot()
        print("Found i ! ",str(myEuro.pid))
    except Exception as e :
        print("findI crashed",str(e))



        
def shakeAndFindP(myEuro, updateThread, maxTime=600, startingP = 1450, endingP = 100, stepP=-50, stepTemp=5):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 0
        
            
        for p in range(startingP, endingP, stepP):
            myEuro.setpoint = myEuro.setpoint + stepTemp
            
            if not keepgoing: break
            print("P", p)
            myEuro.P = p
            updateThread.pidPlot()
            starttime=time.time()
            while time.time() - starttime < maxTime and keepgoing:
                time.sleep(10)
                print("Remaining time ", maxTime- (time.time() - starttime), myEuro.pid, myEuro.setpoint)
        updateThread.pidPlot()
        print("Found p ! ",str(myEuro.pid))
    except Exception as e :
        print("findP crashed",str(e))

    
def autotuneRamp(myEuro, updateThread, startingTemp = 50, endingTemp = 500, step=50):

    global keepgoing
    try:
        myEuro.Ramp_Rate_Disable = 1
        
            
        for temp in range( startingTemp , endingTemp , step):
            myEuro.setpoint = temp
            
            if not keepgoing: break

            print("Autotune start for {temp}".format(temp=temp))
            myEuro.Autotune_enable = 1

            time.sleep(10)
            auto = 1
            while auto and keepgoing:
                try:
                    auto = myEuro.Autotune_enable
                    if auto == 0:
                        time.sleep(5)
                        auto = myEuro.Autotune_enable
                except Exception as e:
                    print("Pbl while reading autotune parameters",str(e))
                time.sleep(1)

            updateThread.pidPlot()        
            print("Autotune done for {temp}, pid={pid}".format(temp=temp, pid=str(myEuro.pid)))

            time.sleep(60)

            
    except Exception as e :
        print("Autune ramp crashed",str(e))

def manualTempVsTension(myEuro, updateThread):    
    # Definition of the tension vs max temperature:
    
    myEuro.manual = True
    myEuro.power = 0

    updateThread.updateTime=5
    
    while myEuro.temperature > 40.0:
        time.sleep(10)
        print("Cooling to 40 degC", myEuro.temperature)

    updateThread.updateTime=0.5

    for power in range(0,99,1):
        myEuro.power = power/10.0
        time.sleep(60)
        print("power output vs temperature", power/10.0, myEuro.temperature)
        updateThread.pidPlot(text=str(power/10.0))
        

    
    myEuro.power = 0


def yoyo(myEuro,mini, maxi, delay):
    while True:
        myEuro.setpoint = mini
        time.sleep(delay)
        myEuro.setpoint = maxi
        time.sleep(delay)

        
    
    
if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    myEuro.Adaptive_tune_enable=0
    myEuro.Autotune_enable = 0




    global app
    app = qt.QApplication([])


    mySilxEurotherm = eurotherm.silxEurotherm(myEuro)
    # Create the thread that calls ThreadSafePlot1D.addCurveThreadSafe
    updateThread = eurotherm.UpdateThread(mySilxEurotherm, updateTime=0.1)

    # open silx window
    mySilxEurotherm.show()
    
    updateThread.start()  # Start updating the plot

    time.sleep(2)
    updateThread.pidPlot()


    # PREPARATION FOR RAFAL ID26
    # THERMOCOAX WITH FRAME SUPPORT FOR OPEN CAPILLARIES


    # python3 euroTools.py -t 39 /dev/ttyUSB0
    # power supply 1A 31.5V


    # In air with the washer tower !
    # In air with washers : 530 degC max.
    
    myEuro.pid=(90,50,3) # ok 50 - 530 degC
    #myEuro.rampRate = 5 # 0.3 deg Over shoot
    #myEuro.rampRate = 10 # 0.4 deg Over shoot
    myEuro.rampRate = 15 # 0.6 deg Over shoot
    #myEuro.rampRate = 20 # 1.5 deg Over shoot

    
