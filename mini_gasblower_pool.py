import time
import eurotherm

if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")


    # Initial pids (25/01/2018)
    #myEuro.pid = (250, 200, 5)
    #myEuro.pid2 = (120, 62, 2)
    
    #myEuro.Output_rate_limit = 0 # Initially

    #Autotune at 80degC:
    # myEuro.pid = (613, 83, 13) # Beurk


    
    # TEst a 100deg C. Minigas blower pointing upward.
    # Ponly to get a decent P/P2 :
    # large amplitude oscilation <3500
    # Smaller oscilation with temperature lower than setpoint:
    # (4000, 0,0)
    # Oscilation period = 56 sec.
    # (4000, 56,0)  # No oscilations. temp too low first (-3degC) then rising up and stabilising smoothlyl..
    # Then after 200 sec, stability 0.4C @ 100C

    # 100C => 200C overshoot of 40C.
    # D set to compensate slope. has to be smaller than an oscilation period. Too small it creates new oscilation.
    # (4000, 56,1) is nice up to 200C

    #200 => 250  increase is very slow. Time to push P a bit. 1000 is still slow to ramp up. 100 is crazy. 500 oscilate, 800 oscilate 1000 oscilate, 2000 perfect
    # => (2000, 56,1)

    # 250 => 300C
    # Still too slow to heat up.
    # P=1500
    
    # new oscilation period of 39 => (4000, 112, 0)
    
    # 300 => 950 with 30degC/min ramp with pid = (4000, 112, 0)
    # give perfect results.
    # stability at 950 is +/-0.3C


    # To reclean a mini gas blower :
    
    myEuro.Output_rate_limit = 2000
    myEuro.rampUnit = 'Seconds'
    myEuro.rampRate = 30
    myEuro.pid = (4000, 112, 0)
    myEuro.pid2 = (4000, 112, 0)

    
