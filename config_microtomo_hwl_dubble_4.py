import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")

    # Request, microtomo Hwl in transmission RT-500 deg in air for Alessandro. 
    # Will use 5 or 10 deg ramprate.
    
    
    # microtomo :
    # Max V/I : 7,4V 11A

    
    myEuro.Output_rate_limit = 2000


    #####  GOOD ONES : ######
    # Tension range (0, 5) # 9A max on PS.
    myEuro.tensionRange = (0,5) # 7.1V 
    # Output_rate_limit = 1000
    myEuro.pid = (33, 60, 10) # @ 550degC 
    # At 200C, +/-0.03K, few peaks of 40mK
    # undershoot of 3C when going down.
    
    

    

    myEuro.rampRate = 25 # 
