import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    myEuro.rampRate = 0

    # Henri swapped the K thermocouple for a R.
    # myEuro.temperatureSensor = 'R'
    #myEuro.tensionRange = (0,30)
    
    
    # Delta electronika :
    # Regulation asked for 
    # HLP limited it to 10A (which is 15V) 
    # 600 degC ok @10A
    # 700 degC OK @10A
    # 1200 degC at 35V 11.2A
    # 
    #Autotune at 1200 degC :
    myEuro.pid = (29, 40, 6)
    
