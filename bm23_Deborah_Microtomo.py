import time
import eurotherm

if __name__ == "__main__":

    global myEuro
    #myEuro = eurotherm.eurotherm2408("/dev/ttyAMA0")
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    # Experiment on BM23 / ID24  Two furnaces, insitu thermocoax catalyse cell and 1 microtomo in transmission.

    # microtomo :
    #myEuro.tensionRange = (0, 5)

    # myEuro.rampRate = 20
    # (27, 135, 7)


    # Works very nice at 250:
    # (+/-0.1 overnight) 
    # no overshoot
    # very slow to reach 350.
    # myEuro.pid = (96, 309, 0)

    # faster response :
    # P only = 20
    # @ 450 degC : oscilations 434/446 period:100sec
    # (100,100,0) => Ok @ 450  Overshoot 0.9, undershoot 1.4
    # @600 : random peaks +/-3degC  with P contribution.

    # (200,100,0) ...
    # 100, 100, 0  Oscilations
    # 100, 400, 0
    #test PI AT at 610 :  67,135,0
    #test PID AT at 610:  (34, 133, 22) 
    #test PID AT at 260:  (52, 293,48)
    # OK at 270, 300
    # test at 400 OK at 400C
    

    # Final 
    # far from perfection:
    myEuro.pid = (52, 293, 48)
    # -0.4 -0.2 @600  OK @300/400 
    
    
    
