import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True


if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Autotune_enable = 0

    # Configuration for ID10 (Bea)
    # Thermocoax inside a transmission sample holder with pellet
    # Radiation screen
    # exp 900C

    # myEuro.tensionRange = (0, 50)  ## Microtomo thermocoax => 31.5V 1A.
    # myEuro.tensionRange = (0, 20) #  =>30V Table1 => 688C max 
    # myEuro.tensionRange = (0, 21) #  =>32V Table1
    
    # myEuro.tensionRange = (0, 22) #  => [ +33.5V HardLim Table1]  730C    
    # myEuro.tensionRange = (0, 23) #  => [+34.5V HardLim Table1]  743C    
    # myEuro.tensionRange = (0, 24) #  => [+35.8V (1.2A) HardLim Table1]  760C    
    # myEuro.tensionRange = (0, 27) #  => [+38.2V hard lim (1.3A)  ] 790C
    # myEuro.tensionRange = (0, 30) # [ + 40V hard lin 1.4A ]  => 820C
    # myEuro.tensionRange = (0, 30) # [ + 42V hard lin 1.4+ A ]  => 850C
    # myEuro.tensionRange = (0, 31) # [ + 45.1V hard lim 1.5 A ]  => 900C Unstabilisable
    
    # myEuro.tensionRange = (0, 31) # [ + 46V hard lim 1.6 A ]  => OK for 900 stab 
    myEuro.tensionRange = (0, 32) # [ + 46V hard lim 1.6 A ]  => OK for 900 stab 
    
    

    myEuro.Output_rate_limit = 2000

    myEuro.rampRate = 20
    
    #Old pids from previous ID10 config:
    #myEuro.pid=(90,50,3)

    #Autotune @ 500C
    #myEuro.pid=(44, 92, 15)

    
    # Autotune at 900 C
    #myEuro.pid=(74, 34, 5)  # <+========

    # 60mK stability :
    # ID10_thermocoax_bea_900degC_01.png

    # Very bad at low temperature (30deg jumps) 

    
    # Autotune at 200 C
    # myEuro.pid=(134, 353, 58)  # Smooth (too smooth?) but slow, 2degC overshoot at 300C (302C)

    # Stable a 300:
    # (110, 353, 58)

    # Smoother : myEuro.pid = (134, 353, 5)
    # Very mou at 700 but ok in the end myEuro.pid = (134, 200, 5)

    # Test From RT to 900 with :
    #myEuro.pid = (110, 200, 5)
    # RT to ramp catch up : nice and smooth, no oscilation.
    # ID10_thermocoax_bea_900degC_ramp_RT-900_110-200-5.png
    # Bad regulation around 880C 0.8C oscilation
    #myEuro.pid = (110, 200, 5) # For temp during ramp <880
    # Changing to :
    myEuro.pid = (250, 200, 5)  # For 900C

    # Dash red is setpoint, dash green the working setpoint (ramp). solid blue is the thermocouple.
    # recommended pids that are a litlle bit slow but work fine:
    # P250, I200, D5
    # Ramp 15 deg C / min.
