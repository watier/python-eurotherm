import time
import eurotherm

    
if __name__ == "__main__":

    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB0")

    # Heater is custom kanthal. Has the flat hat on top, and the ID03 copper plate above for smoothing . 
    # Restriction to 8V 6A for 350C easily
    myEuro.tensionRange = (0,6)
    
    #  @250 in air: 
    myEuro.pid = (12, 104, 17)
