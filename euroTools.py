#! /usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import eurotherm
import pickle
import time


def compareTwoDict(dict1, dict2):
    diffkeys = [k for k in dict1 if dict1[k] != dict2[k]]
    for k in diffkeys:
        print( k, ':  ', dict1[k], ' =/= ', dict2[k])

def flashWithDict(eurotherm, dict):
    for param in dict.keys():
        print("Writing {p} with value {v}".format(p=param, v=dict[param]))
        eurotherm.__setattr__(param, dict[param])
        



    

def saveConfig(dictionaryOfParameters, fileName=None):
    if fileName==None:
        fileName=time.strftime("/tmp/%Y-%m-%d_%H-%M_eurotherm_config_save.p")
    try:
        pickle.dump(dictionaryOfParameters, file(fileName, 'w'))
    except Exception as e:
        print("Impossible to save the configuration file of this eurotherm "+str(e))

    





def findMaxRR(myEuro, delta):
    myEuro.Ramp_Rate_Disable = 1

    startingTemperature = myEuro.temperature
    startingTime = time.time()

    setpoint = startingTemperature + delta
    myEuro.setpoint = setpoint

    temp = myEuro.temperature
    
    while temp <= setpoint :
        temp = myEuro.temperature
        print ("Current temperature = {t} ({deltaT} deg in {deltaS} sec (rate={rate}). ".format(
            t=temp,
            deltaT=temp-startingTemperature,
            deltaS=time.time()-startingTime,
            rate=(temp-startingTemperature)/(time.time()-startingTime)))
        time.sleep(1)
        
    setpoint = startingTemperature
    myEuro.setpoint = setpoint
    
    startingTemperature = myEuro.temperature
    startingTime = time.time()

    while temp >= setpoint :
        temp = myEuro.temperature
        print ("Current temperature = {t} ({deltaT} deg in {deltaS} sec (rate={rate}). ".format(
            t=temp,
            deltaT=temp-startingTemperature,
            deltaS=time.time()-startingTime,
            rate=(temp-startingTemperature)/(time.time()-startingTime)))
        time.sleep(1)

    
    myEuro.Ramp_Rate_Disable = 0



def configOutputForDE(myEuro, desiredMaxTension=7.4, onePcOoutput=71.7, hundredPcOutput=6.9, guess=False):
        print("Attempting configuration for a tension of {tension}".format(tension=desiredMaxTension))
        print("Before configuration\n",\
              "Module identity: ", myEuro._1A_Module_identity,\
              " Module Function: ",myEuro._1A_Module_function,\
              " Retran Min: ",myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
              " Retran Max: ",myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
              " Units: ",myEuro._1A_Units,\
              " Min elec out: ",myEuro._1A_Minimum_electrical_output,\
              " Max Elec out: ",myEuro._1A_Maximum_electrical_output,\
              " Sens: ",myEuro._1A_Sense_of_output,\
              " 1AConf: ",myEuro._1A_Summary_output_1A_configuration,\
              " 1A Telemetry: ",myEuro._1A_DC_output_1A_telemetry_parameter,\
              " 1A Config: ",myEuro._1A_Program_summary_output_1A_config)
        

        # Eurotherm in manual full output.
        # Give the max tension:
        # value = 1; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        # if too low (should be close to max):
        # myEuro.tensionRange = (0.0,50.0)
        # Give the min tension:
        # value = 100; myEuro.Instrument_Mode = 2; myEuro._1A_PID_or_Retran_value_giving_max__o_p = value; myEuro.Instrument_Mode = 0
        if not guess:
            guess = int( (onePcOoutput - desiredMaxTension) * (onePcOoutput - hundredPcOutput)/99.0) - 2
            
        
        print("Guess {guess}% output max with 1%{onePcOoutput} 100%{hundredPcOutput} and {desiredMaxTension}V".format(guess=guess, onePcOoutput=onePcOoutput, hundredPcOutput=hundredPcOutput, desiredMaxTension=desiredMaxTension))

        myEuro.Instrument_Mode = 2
        myEuro._1A_PID_or_Retran_value_giving_min__o_p = 0
        #myEuro._1A_PID_or_Retran_value_giving_max__o_p = guess
        #myEuro._1A_PID_or_Retran_value_giving_max__o_p = 90  # 90 for microtomo in tension regulation  ('BEFORE', None, 17, 0, 90, 1, 0, 50, 0, 0, 0, 0)
        myEuro._1A_PID_or_Retran_value_giving_max__o_p = 100
        
        myEuro.Low_power_limit = 0
        myEuro.High_power_limit = 1000
        myEuro.Remote_low_power_limit = 0
        myEuro.Remote_high_power_limit =1000
        myEuro._1A_Minimum_electrical_output = 0
        myEuro._1A_Maximum_electrical_output = guess # Why 50 ?   
        myEuro.Instrument_Mode = 0
        time.sleep(8)

        
        print("After configuration\n",\
              "Module identity: ", myEuro._1A_Module_identity,\
              " Module Function: ",myEuro._1A_Module_function,\
              " Retran Min: ",myEuro._1A_PID_or_Retran_value_giving_min__o_p,\
              " Retran Max: ",myEuro._1A_PID_or_Retran_value_giving_max__o_p,\
              " Units: ",myEuro._1A_Units,\
              " Min elec out: ",myEuro._1A_Minimum_electrical_output,\
              " Max Elec out: ",myEuro._1A_Maximum_electrical_output,\
              " Sens: ",myEuro._1A_Sense_of_output,\
              " 1AConf: ",myEuro._1A_Summary_output_1A_configuration,\
              " 1A Telemetry: ",myEuro._1A_DC_output_1A_telemetry_parameter,\
              " 1A Config: ",myEuro._1A_Program_summary_output_1A_config)
        



        
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Use eurotherm on a modbus serial line.')

    parser.add_argument('serial_line', help='Which serial line is the eurotherm connected on, something like /dev/tty? /dev/ttyRP2 ...')


    parser.add_argument("-v", "--verbose", help="Print requests on the serial line", action="store_true")
    
    parser.add_argument("-d", "--dump", help="Print all parameters from eurotherm", action="store_true")
    
    parser.add_argument("-s", "--save-in-file",  help="Save eurotherm configuration in a specific file")
    
    parser.add_argument("-c", "--compare-with-file",  help="Compare ther eurotherm configuration with a specific file")
    
    parser.add_argument("-f", "--flash-with-file",  help="flash the eurotherm configuration with a specific file")
    
    parser.add_argument("-r", "--find-max-ramprate",  help="Will go up fast of an amount of temp passed as argument, then go down fast.")
    
    parser.add_argument("-t", "--config-delta",  help="Configure the output of the eurotherm to drive a delta electronica to the desired tension. ")

    parser.add_argument("-m", "--micro-hwl",  help="Configure the output of the eurotherm to drive a delta electronica for a microtomo HWL", action="store_true")

    parser.add_argument("-M", "--micro-thermocoax",  help="Configure the output of the eurotherm to drive a delta electronica for a microtomo thermocoax 31.5V 1A", action="store_true")

    parser.add_argument("-x", "--max-retran",  help="Configure the output of the eurotherm to drive a delta electronica with this max retrans value. ")

    
    
    args = parser.parse_args()

    print("attempting connection on a Eurotherm through modbus on {serial}.".format(serial=args.serial_line))
    myEuro = eurotherm.eurotherm2408(args.serial_line)
    
    if args.verbose:
        print("verbosity turned on")
        myEruro.debugPrint = True

    if args.dump:
        print("Please wait during acquisition of all parameters")
        for (param, value) in myEuro.dumpAll().items():
            print("{param} : {value}".format(param=param, value=value) )


    if args.save_in_file:
        print("Please wait during acquisition of all parameters")
        dump = myEuro.dumpAll()
        print("Saving in {file} (pickle) with one dictionary.".format(file=args.save_in_file))        
        saveConfig(dump, args.save_in_file)

    if args.compare_with_file :
        try: 
            pkl_file = open(args.compare_with_file, 'rb')
            print("Loading saved configuration from "+args.compare_with_file)
            storedConfig = pickle.load(pkl_file)
            print("Please wait during acquisition of all parameters")
            dump = myEuro.dumpAll()        
            print("Configuration Saved // Current configuration")
            compareTwoDict(storedConfig,dump)
        except Exception as e:
            print("Impossible to compare  the configuration file of this eurotherm from a previously saved one: "+str(e))

            


    if args.flash_with_file :
        try:
            pkl_file = open(args.flash_with_file, 'rb')
            storedConfig = pickle.load(pkl_file)
            print("Loading saved configuration from "+args.flash_with_file)
            
            flashWithDict(myEuro, storedConfig)

                          
        except Exception as e:
            print("Could not flash: "+str(e))


    if args.find_max_ramprate:
        findMaxRR(myEuro, int(args.find_max_ramprate))
            
            

    if args.config_delta:
        configOutputForDE(myEuro, desiredMaxTension=float(args.config_delta), onePcOoutput=71.7, hundredPcOutput=0)

    if args.max_retran:
        configOutputForDE(myEuro, guess=float(args.max_retran))
         
    if args.micro_hwl:
        configOutputForDE(myEuro, desiredMaxTension=7.4 , guess=95)
        myEuro.pid = (700, 278, 46)
        print("7.4V 11A MAX")
        
    if args.micro_thermocoax:
        configOutputForDE(myEuro, desiredMaxTension=31.5 , guess=21)
        #myEuro.pid = (700, 278, 46)
        print("MAX CURRENT : 1A") 
        
    # Microtomo thermocoax : 31.5V 1A


    
    # 1000 = 100% !!!!
    # myEuro.High_power_limit = 1000

    
    # For ID11 (HLP)
    # myEuro._1A_Maximum_electrical_output = 20 en regulation courant => 10.2A.
