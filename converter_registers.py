import re

failed = 0
i=0
nb=0

registers = {}
    
with open('registers_nanodac.txt', 'r') as f:
    for line in f.readlines():
        i+=1
        #print("treating : " +line)
        #print("\n")
        m=re.search('(\S+) (.*) (float32|uint8|bool|time_t|string_t|int32|int16|eint32) (\S+) (\S+) (.*)',line)
        if m:

            name = m.group(1).replace('.', '_')

            registers[name] = { "registerDec" : m.group(5),
                                "registerHex" : m.group(4),
                                "type" :        m.group(3),
                                "description" : m.group(2),
                                "resolution"  : m.group(6) }
                                
                                
            
            
            nb+=1
        
        # if not m and line != "" and line !="\n":
        #     failed+=1
        #     print(str(i)+line)

        
# 6137 parameters for nanodac
