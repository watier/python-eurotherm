import templogger
import matplotlib.pyplot as plt
import time
import eurotherm
import threading

# For guis
from silx import sx
import matplotlib.pyplot as plt
import numpy
from silx.gui import qt
from silx.gui.plot import Plot1D
#qapp = qt.QApplication([])

keepgoing = True
    
if __name__ == "__main__":

    global myEuro
    myEuro = eurotherm.eurotherm2408("/dev/ttyUSB1")
    #myEuro.Adaptive_tune_enable=0
    #myEuro.Autotune_enable = 0


    # Configuration of a new inhouse heater made of 0.7mm Tungsten wire inside a 1" button ceramic heater
    # Goal is to have a 1050 degC on a silicon sample
    # Geometry is reflection .

    # # Heater is limitted at 22V  (14A) Regulation is tension.
    # myEuro.temperatureSensor = 'S'

    # myEuro.tensionRange = (0, 16) # => 22V  

    # myEuro.Instrument_Mode = 2;
    # myEuro.Setpoint_Max___High_range_limit = 1400
    # myEuro.Instrument_Mode = 0;

    # time.sleep(10)
    
    # # myEuro.Output_rate_limit = 0
    # # myEuro.manual = False
    # # myEuro.power = 0   
    
    # myEuro.Output_rate_limit = 1000
    # myEuro.rampRate = 20
    
    # myEuro.setpoint = 20    
    # # myEuro.manual = False
    
    # # myEuro.pid = (91, 664, 110) # Automatic at 100C

    # # Good at high Temp :
    # #myEuro.pid = (30, 25, 3)      # Alamano @800C
    

    # # Automatic at 700C :
    # myEuro.pid = (112, 52, 8)
